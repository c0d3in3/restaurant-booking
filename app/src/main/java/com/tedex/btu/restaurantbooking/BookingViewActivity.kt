package com.tedex.btu.restaurantbooking

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.renderscript.Script
import android.support.v4.app.DialogFragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.DatePicker
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_booking_view.*
import java.util.*

class BookingViewActivity : AppCompatActivity() {


    private val bookingList = ArrayList<Booking>()
    private lateinit var recyclerView: RecyclerView
    private lateinit var mAdapter: BookingAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_view)

        recyclerView = findViewById<View>(R.id.recyclerView_Bookings) as RecyclerView

        mAdapter = BookingAdapter(bookingList)
        val mLayoutManager = LinearLayoutManager(applicationContext)
        recyclerView.setLayoutManager(mLayoutManager)
        recyclerView.setItemAnimator(DefaultItemAnimator())
        recyclerView.setHasFixedSize(true)
        recyclerView.setAdapter(mAdapter)

        init()

    }

    private fun init() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val oMonth = month +1

        dateTextView.text = "TODAY"
        getDataFromDatabase("$day/$oMonth/$year")

        datePickerButton.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, mYear, mMonth, mDay ->
                var mon:Int = mMonth+1
                if (mYear == year && mMonth == month && mDay == day) {
                    dateTextView.text = "TODAY"
                } else {
                    dateTextView.text = "$mDay/$mon/$mYear"
                }
                bookingList.clear()
                getDataFromDatabase("$mDay/$mon/$mYear")
            }, year, month, day)
            dpd.show()
        }

        adminPanelButton.setOnClickListener {
            val intent = Intent(this, AdminPanelActivity::class.java)
            startActivity(intent)
        }

        exitButton.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(this, UserPanelActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun getDataFromDatabase(date:String) {
        val db = FirebaseFirestore.getInstance()
        val auth = FirebaseAuth.getInstance()
        val user = auth.currentUser
        var user_CompanyId: Long?
        if (user != null) {
            db.collection("users").document(user.uid)
            .get()
            .addOnSuccessListener { result ->
                Log.d("GetUsersData", user.uid + " => " + result.data)
                user_CompanyId = result.getLong("user_company_id")
                db.collection("bookings").get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (document in task.result!!) {
                            if (document.getLong("Booking_CompanyId") == user_CompanyId && date == document.getString("Booking_Date")) {
                                Log.d("getBooking", document.id + " => " + document.data)
                                val booking = Booking(document.getLong("Booking_Id"), document.getString("Booking_UserInfo"), document.getString("Booking_Date"), document.getLong("Booking_CompanyId"), document.getString("Booking_Status"))
                                bookingList.add(booking)
                            }

                        }
                        mAdapter.notifyDataSetChanged()
                    }
                    else {
                        Log.d("getBooking", "Error getting documents.", task.exception)
                    }
                }
                .addOnFailureListener { exception ->
                    Log.w("GetUsersData", "Error getting documents.", exception)
                }
            }
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, UserPanelActivity::class.java)
        startActivity(intent)
        finish()
    }
}
