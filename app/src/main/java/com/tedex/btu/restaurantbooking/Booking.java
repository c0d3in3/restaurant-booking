package com.tedex.btu.restaurantbooking;

public class Booking {
    private Long Booking_Id;
    private String Booking_UserInfo;
    private String Booking_Date;
    private Long Booking_CompanyId;
    private String Booking_Status;

    public Booking() {
    }

    public Booking(Long booking_Id, String booking_UserInfo, String booking_Date, Long booking_CompanyId, String booking_Status) {
        Booking_Id = booking_Id;
        Booking_UserInfo = booking_UserInfo;
        Booking_Date = booking_Date;
        Booking_CompanyId = booking_CompanyId;
        Booking_Status = booking_Status;
    }

    public Long getBooking_Id() {
        return Booking_Id;
    }

    public void setBooking_Id(Long booking_Id) {
        Booking_Id = booking_Id;
    }

    public String getBooking_UserInfo() {
        return Booking_UserInfo;
    }

    public void setBooking_UserInfo(String booking_UserInfo) {
        Booking_UserInfo = booking_UserInfo;
    }

    public String getBooking_Date() {
        return Booking_Date;
    }

    public void setBooking_Date(String booking_Date) {
        Booking_Date = booking_Date;
    }

    public Long getBooking_CompanyId() {
        return Booking_CompanyId;
    }

    public void setBooking_CompanyId(Long booking_CompanyId) {
        Booking_CompanyId = booking_CompanyId;
    }

    public String getBooking_Status() {
        return Booking_Status;
    }

    public void setBooking_Status(String booking_Status) {
        Booking_Status = booking_Status;
    }
}
