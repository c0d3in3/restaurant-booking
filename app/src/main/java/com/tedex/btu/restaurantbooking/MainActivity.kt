package com.tedex.btu.restaurantbooking

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBar
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var topViewPager: WrappingViewPager
    private lateinit var topViewPagerAdapter: TopViewPagerAdapter
    private lateinit var restaurantViewPager: WrappingViewPager
    private lateinit var restaurantViewPagerAdapter: RestaurantViewPagerAdapter
    private val restaurantsList = ArrayList<Restaurant>()
    private val topList = ArrayList<Restaurant>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        auth = FirebaseAuth.getInstance()

        init()
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if(currentUser == null){
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun init(){
        loginButton.setOnClickListener {
            val intent = Intent(this, UserPanelActivity::class.java)
            startActivity(intent)
            finish()
        }

        createTopAdapter()

        createRestaurantsAdapter()
    }

    private fun createTopAdapter(){

        topViewPager = findViewById(R.id.topViewPager)
        topViewPagerAdapter = TopViewPagerAdapter(this, topList)
        topViewPager.adapter = topViewPagerAdapter

        val db = FirebaseFirestore.getInstance()
        db.collection("restaurants")
        .get()
        .addOnSuccessListener { result ->
            for (document in result) {
                if(document.getBoolean("isTop") == true){
                    Log.d("getTop", document.id + " => " + document.data)
                    val item = Restaurant(document.getLong("id"), document.getString("name"), document.getString("title"), R.drawable.khidi_logo)
                    topList.add(item)
                    Log.d("size", "" + topList.size)
                    topViewPagerAdapter.notifyDataSetChanged()
                }
            }
        }
        .addOnFailureListener { exception ->
            Log.d("getTop", "Error getting documents: ", exception)
        }

    }

    private fun createRestaurantsAdapter(){
        restaurantViewPager = findViewById(R.id.restaurantViewPager)

        restaurantViewPagerAdapter = RestaurantViewPagerAdapter(this, restaurantsList)
        restaurantViewPager.adapter = restaurantViewPagerAdapter


        val db = FirebaseFirestore.getInstance()
        db.collection("restaurants")
        .get()
        .addOnSuccessListener { result ->
            for (document in result) {
                if(document.id != "id_number"){
                    Log.d("getRestaurant", document.id + " => " + document.data)
                    val item = Restaurant(document.getLong("id"), document.getString("name"), document.getString("title"), R.drawable.khidi_logo)
                    restaurantsList.add(item)
                    Log.d("size", "" + restaurantsList.size)
                    restaurantViewPagerAdapter.notifyDataSetChanged()
                }
            }
        }
        .addOnFailureListener { exception ->
            Log.d("getRestaurant", "Error getting documents: ", exception)
        }
    }
}
