package com.tedex.btu.restaurantbooking

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_user_panel.*
import java.util.ArrayList

class UserPanelActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var restaurantViewPager: WrappingViewPager
    private lateinit var restaurantViewPagerAdapter: RestaurantViewPagerAdapter
    private val restaurantsList = ArrayList<Restaurant>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_panel)

        auth = FirebaseAuth.getInstance()

        init()

        createFavoriteAdapter()
    }

    @SuppressLint("SetTextI18n")
    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if(currentUser == null){
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        val db = FirebaseFirestore.getInstance()
        db.collection("users").document(currentUser!!.uid).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    helloTextView.text = "Hello " + document.getString("user_firstname") + "!"
                    userInfoTextView.text = document.getString("user_firstname") + " " + document.getString("user_lastname")
                    val bookings = document.getString("user_bookings")
                    if(!bookings!!.isEmpty()) {
                        val splitted = bookings.split(",".toRegex())
                        bookingTextView.text = splitted.size.toString() + "\nBookings"
                    }


                    Log.d("getUserData", "DocumentSnapshot data: " + document.data)
                } else {
                    Log.d("getUserData", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d("getUserData", "get failed with ", exception)
            }
    }


    private fun init(){
        homeButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
        loginButton.setOnClickListener {
            val intent = Intent(this, BookingViewActivity::class.java)
            startActivity(intent)
            finish()
        }

        exitButton.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun createFavoriteAdapter(){
        restaurantViewPager = findViewById(R.id.favoriteViewPager)

        restaurantViewPagerAdapter = RestaurantViewPagerAdapter(this, restaurantsList)
        restaurantViewPager.adapter = restaurantViewPagerAdapter


        val db = FirebaseFirestore.getInstance()
        db.collection("restaurants")
        .get()
        .addOnSuccessListener { result ->
            for (document in result) {
                if(document.id != "id_number"){
                    Log.d("getRestaurant", document.id + " => " + document.data)
                    val item = Restaurant(document.getLong("id"), document.getString("name"), document.getString("title"), R.drawable.khidi_logo)
                    restaurantsList.add(item)
                    Log.d("size", "" + restaurantsList.size)
                    restaurantViewPagerAdapter.notifyDataSetChanged()
                }
            }
        }
        .addOnFailureListener { exception ->
            Log.d("getRestaurant", "Error getting documents: ", exception)
        }

    }

    override fun onBackPressed() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
