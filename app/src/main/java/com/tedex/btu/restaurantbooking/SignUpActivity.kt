package com.tedex.btu.restaurantbooking

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sign_up.*
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore


class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    var editTextList = ArrayList<EditText>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        auth = FirebaseAuth.getInstance()

        signUpUser()
    }

    private fun signUpUser(){
        signUpButton.setOnClickListener {
            if(!firstNameEditText.text.toString().isEmpty() && !lastNameEditText.text.toString().isEmpty() && !emailEditText.text.toString().isEmpty() &&
                    !passwordEditText.text.toString().isEmpty() && !phoneNumberEditText.text.toString().isEmpty() && !idNumberEditText.text.toString().isEmpty()){
                auth.createUserWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("SignUp", "createUserWithEmail:success")

                        val db = FirebaseFirestore.getInstance()
                        val user = FirebaseAuth.getInstance().currentUser
                        val data = HashMap<String, Any>()
                        data["user_firstname"] = firstNameEditText.text.toString()
                        data["user_lastname"] = lastNameEditText.text.toString()
                        data["user_email"] = emailEditText.text.toString()
                        data["user_id_number"] = idNumberEditText.text.toString().toLong()
                        data["user_phone_number"] = phoneNumberEditText.text.toString().toLong()
                        data["user_favorites"] = ""
                        data["user_bookings"] = ""
                        data["user_company_id"] = 0
                        db.collection("users").document(user!!.uid).set(data)
                            .addOnSuccessListener { Log.d("SignUp", "DocumentSnapshot successfully written!") }
                            .addOnFailureListener { e -> Log.w("SignUp", "Error writing document", e) }
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.d("SignUp", "createUserWithEmail:failure", task.exception)
                        Toast.makeText(baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                    }
                }
            }
            else{
                Toast.makeText(this, "Please fill all the fields!", Toast.LENGTH_LONG).show()
            }
        }
    }
}
