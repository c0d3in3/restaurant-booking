package com.tedex.btu.restaurantbooking;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;


public class WrappingViewPager extends ViewPager {

    public WrappingViewPager(@NonNull Context context) {
        super(context);
    }

    public WrappingViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
        super.onMeasure(widthMeasureSpec,heightMeasureSpec);
    }
}