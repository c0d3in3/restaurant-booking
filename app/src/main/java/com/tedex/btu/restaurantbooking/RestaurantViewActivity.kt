package com.tedex.btu.restaurantbooking

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_restaurant_view.*
import java.util.*

class RestaurantViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant_view)
        getRestaurantData()

        init()
    }

    private fun init(){
        restaurantBook.setOnClickListener {
            val restaurantId = intent.getLongExtra("restaurantId", 0)
            val auth = FirebaseAuth.getInstance()
            val db = FirebaseFirestore.getInstance()
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH) + 1
            val day = c.get(Calendar.DAY_OF_MONTH)
            val booking = HashMap<String, Any?>()

            db.collection("restaurants").document(restaurantId.toString()).get().addOnSuccessListener { document ->
                if (document != null) {
                    Log.d("getDocument", "DocumentSnapshot data: " + document.data)
                    val bookingId = document.getLong("booking_id")
                    booking["Booking_CompanyId"] = restaurantId
                    booking["Booking_Date"] = "$day/$month/$year"
                    booking["Booking_Id"] = bookingId!! +1
                    booking["Booking_Status"] = "Waiting"
                    booking["Booking_UserInfo"] = auth.currentUser!!.uid

                    db.collection("restaurants").document(restaurantId.toString()).update("booking_id", bookingId!! +1)

                    Toast.makeText(this, "You have booked successfully!", Toast.LENGTH_LONG).show()
                    db.collection("bookings")
                    .add(booking)
                    .addOnSuccessListener {documentReference ->
                        val documentId = documentReference.id
                        Log.d("InsertBooking", "booking added successfully")
                        db.collection("users").document(auth.currentUser!!.uid).get().addOnSuccessListener { document ->
                            if(document != null){
                                val userBooking = document.getString("user_bookings")
                                if(userBooking!!.isEmpty()){
                                    db.collection("users").document(auth.currentUser!!.uid).update("user_bookings", documentId)
                                }
                                else{
                                    db.collection("users").document(auth.currentUser!!.uid).update("user_bookings", "$userBooking,$documentId")
                                }
                            }
                        }
                    }
                    .addOnFailureListener {
                        Log.d("InsertBooking", "booking failure")
                    }
                } else {
                    Log.d("getDocument", "No such document")
                    Toast.makeText(this, "Booking was unsuccessful!", Toast.LENGTH_LONG).show()
                }
            }
            .addOnFailureListener { exception ->
                Log.d("getDocument", "get failed with ", exception)
                Toast.makeText(this, "Booking was unsuccessful!", Toast.LENGTH_LONG).show()
            }
        }
        homeButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        loginButton.setOnClickListener {
            val intent = Intent(this, UserPanelActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun getRestaurantData(){
        val db = FirebaseFirestore.getInstance();
        val restaurantId = intent.getLongExtra("restaurantId", 0)

        db.collection("restaurants").document(restaurantId.toString()).get().addOnSuccessListener { document ->
            if (document != null) {
                Log.d("getDocument", "DocumentSnapshot data: " + document.data)
                //restaurantImage.setBackgroundResource()
                restaurantName.text = document.getString("name")
                restaurantTitle.text = document.getString("title")
                restaurantRate.text = "Rate: 0.0"
            } else {
                Log.d("getDocument", "No such document")
            }
        }
        .addOnFailureListener { exception ->
            Log.d("getDocument", "get failed with ", exception)
        }
    }
}
