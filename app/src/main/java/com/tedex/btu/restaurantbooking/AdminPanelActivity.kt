package com.tedex.btu.restaurantbooking

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_admin_panel.*

class AdminPanelActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_panel)

        init()
    }

    private fun init(){
        restaurantButton.setOnClickListener {
            val intent = Intent(this, RestaurantEditActivity::class.java)
            startActivity(intent)
            finish()
        }
        /*favoriteButton.setOnClickListener {
            val intent = Intent(this, FavoriteEditActivity::class.java)
            startActivity(intent)
            finish()
        }*/

        backButton.setOnClickListener {
            val intent = Intent(this, BookingViewActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}
