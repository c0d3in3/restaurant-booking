package com.tedex.btu.restaurantbooking;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;

import java.util.List;


public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.MyViewHolder> {

    private List<Booking> bookingList;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public FirebaseAuth mAuth;
        public FirebaseUser mUser;
        public String mUid;
        public FirebaseFirestore db;
        public FirebaseStorage storage;
        public TextView booking_id;
        public TextView booking_userInfo;
        public TextView booking_date;
        public TextView booking_status;


        public MyViewHolder(View view) {
            super(view);
            mAuth = FirebaseAuth.getInstance();
            mUser = mAuth.getCurrentUser();
            mUid = mUser.getUid();
            db = FirebaseFirestore.getInstance();
            storage = FirebaseStorage.getInstance();
            booking_id = (TextView) view.findViewById(R.id.booking_Id);
            booking_userInfo = (TextView) view.findViewById(R.id.booking_UserInfo);
            booking_date = (TextView) view.findViewById(R.id.booking_Date);
            booking_status = (TextView) view.findViewById(R.id.booking_Status);
        }

        public void onClick(View v) {
        }
    }


    public BookingAdapter(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.booking_list_item, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        final Booking booking = bookingList.get(position);
        db.collection("users").document(booking.getBooking_UserInfo()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        String userInfo = document.getString("user_firstname") + " " + document.getString("user_lastname");
                        holder.booking_id.setText("Booking ID: " + booking.getBooking_Id().toString());
                        holder.booking_userInfo.setText("Booked By: " + userInfo);
                        holder.booking_date.setText("Booked on: " + booking.getBooking_Date());
                        holder.booking_status.setText("Booking Status: " + booking.getBooking_Status());
                    } else {
                        Log.d("getUserInfo", "No such document");
                    }
                } else {
                    Log.d("getUserInfo", "get failed with ", task.getException());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookingList.size();
    }

}