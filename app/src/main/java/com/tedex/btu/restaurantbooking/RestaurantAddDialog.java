package com.tedex.btu.restaurantbooking;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.core.Query;

import java.util.HashMap;
import java.util.Map;


public class RestaurantAddDialog extends DialogFragment {

    private EditText restaurantName;
    private EditText restaurantTitle;
    private Button addButton;
    private Button cancelButton;

    interface onDialogUpdate{
        void updateAdapter();
    }

    public onDialogUpdate mOnDialogUpdate;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_restaurant, container, false);
        restaurantName = view.findViewById(R.id.restaurant_nameEditText);
        restaurantTitle = view.findViewById(R.id.restaurant_titleEditText);
        addButton = view.findViewById(R.id.addButton);
        cancelButton = view.findViewById(R.id.cancelButton);

        cancelButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                getDialog().dismiss();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                final FirebaseFirestore db = FirebaseFirestore.getInstance();


                db.collection("restaurants").document("id_number")
                .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            Long id;
                            DocumentSnapshot document = task.getResult();
                            id = document.getLong("id");
                            db.collection("restaurants")
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if (task.isSuccessful()) {
                                                Log.d("size22", task.getResult().size()-1 + "");
                                                db.collection("restaurants").document("id_number").update("id", task.getResult().size()-1);
                                            } else {
                                                Log.d("getID", "Error getting documents: ", task.getException());
                                            }
                                        }
                                    });
                            Map<String, Object> data = new HashMap<>();
                            data.put("id", id+1);
                            data.put("name", restaurantName.getText().toString());
                            data.put("title", restaurantTitle.getText().toString());
                            data.put("isTop", false);
                            data.put("booking_id", 0);
                            db.collection("restaurants").document(String.valueOf(id+1))
                                .set(data)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d("addRestaurant", "DocumentSnapshot successfully written!");
                                        mOnDialogUpdate.updateAdapter();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w("addRestaurant", "Error writing document", e);
                                    }
                                });

                        } else {
                            Log.w("getId", "Error getting documents.", task.getException());
                        }
                    }
                });
                getDialog().dismiss();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mOnDialogUpdate = (onDialogUpdate) getActivity();
        }catch (ClassCastException e){
            Log.e("onAttach", "onAttach: ClassCastException: " + e.getMessage() );
        }
    }
}
