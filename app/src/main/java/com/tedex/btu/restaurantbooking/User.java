package com.tedex.btu.restaurantbooking;

public class User {
    private String userID;
    private Long userCompanyID;
    private Long userIDNumber;
    private String userEmail;
    private String userFirstName;
    private String userLastName;
    private String userFavorites;
    private String userBookings;
    private Long userPhoneNumber ;

    public User() {
    }

    public User(String userID, Long userCompanyID, Long userIDNumber, String userEmail, String userFirstName, String userLastName, String userFavorites, String userBookings, Long userPhoneNumber) {
        this.userID = userID;
        this.userCompanyID = userCompanyID;
        this.userIDNumber = userIDNumber;
        this.userEmail = userEmail;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userFavorites = userFavorites;
        this.userBookings = userBookings;
        this.userPhoneNumber = userPhoneNumber;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Long getUserCompanyID() {
        return userCompanyID;
    }

    public void setUserCompanyID(Long userCompanyID) {
        this.userCompanyID = userCompanyID;
    }

    public Long getUserIDNumber() {
        return userIDNumber;
    }

    public void setUserIDNumber(Long userIDNumber) {
        this.userIDNumber = userIDNumber;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserFavorites() {
        return userFavorites;
    }

    public void setUserFavorites(String userFavorites) {
        this.userFavorites = userFavorites;
    }

    public String getUserBookings() {
        return userBookings;
    }

    public void setUserBookings(String userBookings) {
        this.userBookings = userBookings;
    }

    public Long getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(Long userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }
}

