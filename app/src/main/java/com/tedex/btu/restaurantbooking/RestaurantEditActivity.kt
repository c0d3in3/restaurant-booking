package com.tedex.btu.restaurantbooking

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.Adapter
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_restaurant_edit.*
import java.util.ArrayList

class RestaurantEditActivity : AppCompatActivity(),RestaurantAddDialog.onDialogUpdate{
    override fun updateAdapter() {
        restaurantList.clear()
        getDataFromDatabase()
    }

    private val restaurantList = ArrayList<Restaurant>()
    private lateinit var recyclerView: RecyclerView
    private lateinit var mAdapter: RestaurantAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant_edit)

        recyclerView = findViewById<View>(R.id.recyclerView_Restaurant) as RecyclerView

        mAdapter = RestaurantAdapter(restaurantList)
        val mLayoutManager = LinearLayoutManager(applicationContext)
        recyclerView.setLayoutManager(mLayoutManager)
        recyclerView.setItemAnimator(DefaultItemAnimator())
        recyclerView.setHasFixedSize(true)
        recyclerView.setAdapter(mAdapter)

        init()

        getDataFromDatabase()
    }

    private fun getDataFromDatabase(){
        val db = FirebaseFirestore.getInstance()
        db.collection("restaurants")
        .get()
        .addOnSuccessListener { result ->
            for (document in result) {
                if (document.id == "id_number") {
                    continue
                }
                Log.d("getRestaurant", document.id + " => " + document.data)
                val item = Restaurant(document.getLong("id"), document.getString("name"), document.getString("title"), R.drawable.khidi_logo)
                restaurantList.add(item)
                Log.d("size", "" + restaurantList.size)
                mAdapter.notifyDataSetChanged()
            }
        }
        .addOnFailureListener { exception ->
            Log.d("getRestaurant", "Error getting documents: ", exception)
        }
    }
    private  fun init() {
        restaurantAddButton.setOnClickListener {
            val dialog = RestaurantAddDialog()
            dialog.show(supportFragmentManager, "Add Restaurant")
        }

        backButton.setOnClickListener {
            val intent = Intent(this, AdminPanelActivity::class.java)
            startActivity(intent)
            finish()
        }

        adminPanelButton.setOnClickListener {
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, AdminPanelActivity::class.java)
        startActivity(intent)
        finish()
    }
}
