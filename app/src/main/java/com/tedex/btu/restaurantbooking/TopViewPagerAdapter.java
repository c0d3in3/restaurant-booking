package com.tedex.btu.restaurantbooking;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class TopViewPagerAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Restaurant> topList;

    public TopViewPagerAdapter(Context context, List<Restaurant> topList) {
        this.context = context;
        this.topList = topList;
    }

    @Override
    public int getCount() {
        return topList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.main_view_pager, null);

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        final Restaurant top = topList.get(position);
        final ImageView restaurant_image = (ImageView) view.findViewById(R.id.viewPager_Image);
        restaurant_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId() == restaurant_image.getId()){
                    Intent intent = new Intent(v.getContext(), RestaurantViewActivity.class);
                    intent.putExtra("restaurantId", top.getId());
                    v.getContext().startActivity(intent);
                }
            }
        });
        TextView name = (TextView) view.findViewById(R.id.nameTextView);
        TextView title = (TextView) view.findViewById(R.id.titleTextView);

        name.setText(top.getName());
        title.setText(top.getTitle());

        restaurant_image.setMaxWidth(size.x);
        restaurant_image.setMaxHeight(size.y/3);
        restaurant_image.setImageResource(top.getPicture());

        WrappingViewPager vp = (WrappingViewPager) container;
        ViewGroup.LayoutParams lp = vp.getLayoutParams();
        lp.height = size.y/3;
        vp.requestLayout();
        vp.addView(view, 0);
        return  view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        WrappingViewPager vp = (WrappingViewPager) container;
        View view = (View) object;
        vp.removeView(view);
    }

}
