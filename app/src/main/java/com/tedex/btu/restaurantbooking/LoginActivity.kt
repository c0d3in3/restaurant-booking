package com.tedex.btu.restaurantbooking

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import android.support.v4.view.ViewCompat.setAlpha
import android.widget.LinearLayout
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.activity_main.*


class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()


        init()
    }

    public override fun onStart() {
        super.onStart()
        if(auth.currentUser != null){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun init(){
        logInButton.setOnClickListener {
            if(!emailEditText.text.toString().isEmpty() && !passwordEditText.text.toString().isEmpty()){
                blurAll()
                progressBar.visibility = View.VISIBLE
                auth.signInWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("Log In", "signInWithEmail:success")
                        val user = auth.currentUser
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                        unblurAll()
                        progressBar.visibility = View.INVISIBLE
                        finish()
                    }
                    else {
                        // If sign in fails, display a message to the user.
                        Log.w("Log In", "signInWithEmail:failure", task.exception)
                        progressBar.visibility = View.INVISIBLE
                        unblurAll()
                        Toast.makeText(baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private fun blurAll(){
        logoTextView.alpha = 0.1f
        emailEditText.alpha = 0.1f
        passwordEditText.alpha = 0.1f
        logInButton.alpha = 0.1f
        signUpButton.alpha = 0.1f
    }

    private fun unblurAll(){
        logoTextView.alpha = 1.0f
        emailEditText.alpha = 1.0f
        passwordEditText.alpha = 1.0f
        logInButton.alpha = 1.0f
        signUpButton.alpha = 1.0f
    }
}
