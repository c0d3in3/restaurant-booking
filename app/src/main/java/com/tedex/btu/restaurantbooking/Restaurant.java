package com.tedex.btu.restaurantbooking;

public class Restaurant {
    private Long Id;
    private String Name;
    private String Title;
    private int Picture;

    public Restaurant() {
    }

    public Restaurant(Long id, String name, String title, int picture) {
        Id = id;
        Name = name;
        Title = title;
        Picture = picture;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getPicture() {
        return Picture;
    }

    public void setPicture(int picture) {
        Picture = picture;
    }
}
