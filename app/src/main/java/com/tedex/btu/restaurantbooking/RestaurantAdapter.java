package com.tedex.btu.restaurantbooking;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;

import java.util.List;


public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.MyViewHolder> {

    private List<Restaurant> restaurantList;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView restaurant_Id;
        public TextView restaurant_Name;
        public TextView restaurant_Title;
        public Button deleteButton;
        public FirebaseFirestore db;

        public MyViewHolder(View view) {
            super(view);
            restaurant_Id = (TextView) view.findViewById(R.id.restaurantId);
            restaurant_Name = (TextView) view.findViewById(R.id.restaurantName);
            restaurant_Title = (TextView) view.findViewById(R.id.restaurantTitle);
            deleteButton = (Button) view.findViewById(R.id.deleteButton);
            db = FirebaseFirestore.getInstance();
            deleteButton.setOnClickListener(this);
        }

        public void onClick(final View v) {
            if(v.getId() == deleteButton.getId()){
                Log.d("deleteRestaurant", "Restaurant with id: " + String.valueOf(restaurant_Id.getText()) + " deleted successfully!");
                db.collection("restaurants").document(String.valueOf(restaurant_Id.getText()))
                    .delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("deleteRestaurant", "Restaurant with id: " + String.valueOf(restaurant_Id.getText()) + " deleted successfully!");
                        Intent intent = new Intent(v.getContext(), RestaurantEditActivity.class);
                        v.getContext().startActivity(intent);
                    }
                });
                db.collection("restaurants").document("id_number").update("id", Long.valueOf(String.valueOf(restaurant_Id.getText()))-1);
            }
        }
    }


    public RestaurantAdapter(List<Restaurant> restaurantList) {
        this.restaurantList = restaurantList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.restaurant_edit_layout, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Restaurant restaurant = restaurantList.get(position);
        holder.restaurant_Id.setText(restaurant.getId().toString());
        holder.restaurant_Name.setText(restaurant.getName());
        holder.restaurant_Title.setText(restaurant.getTitle());

    }

    @Override
    public int getItemCount() {
        return restaurantList.size();
    }

}